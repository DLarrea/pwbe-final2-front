import { Medico, } from "./medico";
import { Paciente } from "./paciente";

export class Ficha {
    id: number = null as any;
    fecha: string = null as any;
    motivo: string = null as any;
    diagnostico: string = null as any;
    tratamiento: string = null as any;
    paciente: Paciente = new Paciente();
    medico: Medico = new Medico();
}
export class Medico {
    id: number = null as any;
    cedula: string = null as any;
    nombre: string = null as any;
    apellido: string = null as any;
    telefono: string = null as any;
    email: string = null as any;
    fechaNacimiento: string = null as any;
    usuario: string = null as any;
    password: string = null as any;
    especialidad: string = null as any;
}
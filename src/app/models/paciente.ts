export class Paciente {
    id: number = null as any;
    cedula: string = null as any;
    nombre: string = null as any;
    apellido: string = null as any;
    telefono: string = null as any;
    email: string = null as any;
    fechaNacimiento: string = null as any;
}
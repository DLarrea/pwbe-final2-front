import { TestBed } from '@angular/core/testing';

import { BiomedicService } from './biomedic.service';

describe('BiomedicService', () => {
  let service: BiomedicService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BiomedicService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Ficha } from '../models/ficha';

@Injectable({
  providedIn: 'root'
})
export class BiomedicService {

  constructor(private http: HttpClient) { }

  getFichas():Observable<any>{
    return this.http.get(`/api/v1/ficha`);
  }
  
  getFichasFilter(params:any):Observable<any>{
    
    let p = new HttpParams();
    
    if(params?.medico){
      p = p.append("medico", params?.medico);
    }
    if(params?.especialidad){
      p = p.append("especialidad", params?.especialidad);
    }
    if(params?.paciente){
      p = p.append("paciente", params?.paciente);
    }
    if(params?.fecha){
      p = p.append("fecha", params?.fecha);
    }
    if(params?.motivo){
      p = p.append("motivo", params?.motivo);
    }
    if(params?.diagnostico){
      p = p.append("diagnostico", params?.diagnostico);
    }
    if(params?.tratamiento){
      p = p.append("tratamiento", params?.tratamiento);
    }
    return this.http.get(`/api/v1/ficha`, {params: p});
  }
  
  getPacientes():Observable<any>{
    return this.http.get(`/api/v1/paciente`);
  }
  
  getMedicos():Observable<any>{
    return this.http.get(`/api/v1/medico`);
  }
  
  guardarFicha(ficha:Ficha):Observable<any>{
    return this.http.post(`/api/v1/ficha`, ficha);
  }
}

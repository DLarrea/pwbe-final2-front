import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Ficha } from './models/ficha';
import { Medico } from './models/medico';
import { Paciente } from './models/paciente';
import { BiomedicService } from './services/biomedic.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private api: BiomedicService, private dp: DatePipe) { }

  ngOnInit() {
    this.getFichas();
    this.getMedicos();
    this.getPacientes();

    this.nuevaFicha.fecha = <string>this.dp.transform(new Date(), "yyyy-MM-dd");
  }

  fichas: Ficha[] = [];
  medicos: Medico[] = [];
  pacientes: Paciente[] = [];

  getFichas() {
    this.api.getFichas().subscribe(
      data => {
        this.fichas = data;
        console.log(this.fichas);
      },
      error => console.log
    )
  }

  getMedicos() {
    this.api.getMedicos().subscribe(
      data => {
        this.medicos = data;
        console.log(this.medicos);
      },
      error => console.log
    )
  }

  getPacientes() {
    this.api.getPacientes().subscribe(
      data => {
        this.pacientes = data;
        console.log(this.pacientes);
      },
      error => console.log
    )
  }

  detalle: Ficha = new Ficha();
  verDetalles(target: Ficha) {
    this.detalle = target;
  }

  nuevaFicha: Ficha = new Ficha();

  guardarFicha() {
    if (this.check()) {
      this.api.guardarFicha(this.nuevaFicha).subscribe(
        data => {
          this.getFichas();
          this.nuevaFicha = new Ficha();
          this.nuevaFicha.fecha = <string>this.dp.transform(new Date(), "yyyy-MM-dd");
        }
      )
    }
  }

  check() {
    if (this.nuevaFicha.fecha == null) {
      return false;
    }
    if (this.nuevaFicha.diagnostico == null) {
      return false;
    }
    if (this.nuevaFicha.tratamiento == null) {
      return false;
    }
    if (this.nuevaFicha.motivo == null) {
      return false;
    }
    if (this.nuevaFicha.paciente == null) {
      return false;
    }
    if (this.nuevaFicha.medico == null) {
      return false;
    }
    return true;
  }

  filtros = {
    especialidad: null,
    medico: null,
    paciente: null,
    fecha: null,
    motivo: null,
    diagnostico: null,
    tratamiento: null,
  }

  filtrarResultados() {

    this.api.getFichasFilter(this.filtros).subscribe(
      data => {
        this.fichas = data;
      },
      error => console.log()
    )

  }

  limpiar() {

    this.filtros = {
      especialidad: null,
      medico: null,
      paciente: null,
      fecha: null,
      motivo: null,
      diagnostico: null,
      tratamiento: null,
    }

    this.getFichas();
    
  }
}
